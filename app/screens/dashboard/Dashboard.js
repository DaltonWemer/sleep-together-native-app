import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions
} from 'react-native'

// Third Party Packages
import { FontAwesome } from '@expo/vector-icons'
import { LineChart } from 'react-native-chart-kit'

//Components
import SleepListItem from '../../components/SleepListItem'

// Class swide Variables
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

export default class Dashboard extends React.Component {
  render() {

  const data = {
    labels: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'],
    datasets: [{
      data: [ 5, 7, 10, 12, 4, 9 ],
      strokeWidth: 2 
    }]
  }

  const chartConfig = {
    backgroundGradientFrom: '#FFF',
    backgroundGradientTo: '#FFF',
    color: (opacity = 1) => `rgba(100, 65, 165, ${opacity})`,
  }

    return (
      <View style={styles.container}>
        <LineChart
          data={data}
          width={screenWidth / 1.2}
          height={200}
          chartConfig={chartConfig}
          withInnerLines={false}
          style={styles.lineChart}
          bezier
        />
        <Text style={styles.weekText}>Week at a Glance</Text>
        <ScrollView
          style={styles.list}
        >
          <SleepListItem
            startTime={'12:50 AM'}
            endTime={'9:45 AM'}
            hoursSlept={'8'}
          />
          <SleepListItem
            startTime={'12:50 AM'}
            endTime={'9:45 AM'}
            hoursSlept={'10'}
          />
          <SleepListItem
            startTime={'12:50 AM'}
            endTime={'9:45 AM'}
            hoursSlept={'10'}
          />
          <SleepListItem
            startTime={'12:50 AM'}
            endTime={'9:45 AM'}
            hoursSlept={'10'}
          />
        </ScrollView>

        <TouchableOpacity
          style={styles.eye}
        >
          <FontAwesome
            name="eye"
            size={40}
            color={'#6441A5'}
            style={styles.eyeLogo}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  chartCard:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineChart:{
    marginVertical: 8,
    borderRadius: 16,
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 2 },
    marginTop: 35,
    padding: 50
  },
  list:{
    flex:2,
    width: '100%'
  },
  eye:{
    bottom: 20,
    right: 20,
    borderRadius: 50,
    backgroundColor: '#FFF',
    shadowOpacity: 0.3,
    alignSelf: 'flex-end',
    shadowOffset: { width: 0, height: 2 },
    alignItems: 'center',
    justifyContent: 'center'
  },
  weekText:{
    fontSize: 22,
    color: '#6441A5',
    alignSelf: 'flex-start',
    paddingHorizontal: 20
  },
  eyeLogo:{
    padding: 10
  },

});
