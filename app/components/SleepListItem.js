import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView 
} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'


export default class SleepListItem extends React.Component {
  render() {
    return (
      <View
        style={styles.container}
      >
        <View
          style={{
            flexDirection: 'row'
          }}
        >
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              paddingRight: 10
            }}
          >
            <Text style={styles.supportingText}>Hours</Text>
            <Text style={styles.hoursSlept}>{this.props.hoursSlept}</Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
            }}
          >
            <Text style={styles.supportingText}>{this.props.startTime}</Text>
            <Text style={styles.supportingText}>{this.props.endTime}</Text>
          </View>
        </View>
        <View
          style={{
            justifyContent: 'center'
          }}
        >
          <TouchableOpacity>
            <FontAwesome
              name="pencil"
              size={20}
              color={'gray'}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{  
    justifyContent:'space-between',
    flexDirection: 'row',
    borderBottomWidth:1,
    flex:1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderBottomColor: 'lightgray'
  },
  hoursSlept:{
    color: '#6441A5',
    fontSize: 24
  },
  supportingText:{
    fontSize: 12,
    color: 'gray'
  }
});
