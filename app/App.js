import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions
} from 'react-native'

import LoggedIn from './screens/dashboard/Dashboard'

import { createStackNavigator, createAppContainer } from 'react-navigation'

export default class App extends React.Component {
  state = {
    ready: true,
    initRoute: 'LoggedIn'
  }

 
  
  render() {
    Nav = (initRoute) => {
    return createStackNavigator({
      LoggedIn: { screen: LoggedIn},
    },
      {
        headerMode: 'none'
      }
    )
      
  }
      const AppNav = Nav(this.state.initRoute)
      const App = createAppContainer(AppNav);


      return(<App/>) 
  }

}

